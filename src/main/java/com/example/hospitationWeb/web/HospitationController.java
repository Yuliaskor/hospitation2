package com.example.hospitationWeb.web;

import com.example.hospitationWeb.repository.CoursesRepository;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/")
public class HospitationController {

    private final CoursesRepository coursesRepository;
    private String hospitationPlanPage="/admin/hospitationPlanPage";

    public HospitationController(CoursesRepository coursesRepository) {
        this.coursesRepository = coursesRepository;
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_HOSPITUJACY')")
    @GetMapping("/getHospitationPlan")
    public String hospitationPlan( Model model){

        model.addAttribute("courses", coursesRepository.findAll() );
        return hospitationPlanPage;
    }


}
