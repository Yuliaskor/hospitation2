package com.example.hospitationWeb.web;

import com.example.hospitationWeb.model.Form;
import com.example.hospitationWeb.model.Hospitation;
import com.example.hospitationWeb.repository.HospitationRepository;
import com.example.hospitationWeb.service.HospitationFormService;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import com.example.hospitationWeb.utils.GeneratePdfReport;

import org.springframework.http.HttpHeaders;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("")
public class HospitationFormController {

    private final String hospitationFormPage = "/forms/hospitationForm";
    private final String hospitationResultPage = "/admin/hospitationResult";

    private final HospitationRepository hospitationRepository;

    private HospitationFormService hospitationFormService;


    public HospitationFormController(HospitationRepository hospitationRepository, HospitationFormService hospitationFormService) {
        this.hospitationRepository = hospitationRepository;
        this.hospitationFormService = hospitationFormService;
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_HOSPITUJACY')")
    @GetMapping("/hospitationForm/")
    public String getHospitationForm(@RequestParam(name = "id") String id, Model model) {
        model.addAttribute("id", id);
        model.addAttribute("form", new Form());
        return hospitationFormPage;
    }

    @PostMapping("/addToForm")
    public String registration(@ModelAttribute("form") Form userForm, BindingResult bindingResult) {
        // userValidator.validate(userForm, bindingResult);

        if (bindingResult.hasErrors()) {
            return hospitationFormPage;
        }

        hospitationFormService.saveForm(userForm, 1L);

        return "redirect:/getHospitationPlan";
    }

    //    @GetMapping("/hospitation/export/pdf")
//    public void exportToPDF(HttpServletResponse response) throws DocumentException, IOException {
//        response.setContentType("application/pdf");
//        DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
//        String currentDateTime = dateFormatter.format(new Date());
//
//        String headerKey = "Content-Disposition";
//        String headerValue = "attachment; filename=users_" + currentDateTime + ".pdf";
//        response.setHeader(headerKey, headerValue);
//
//        List<User> listUsers = service.listAll();
//
//        UserPDFExporter exporter = new UserPDFExporter(listUsers);
//        exporter.export(response);
//
//    }
    @RequestMapping(value = "/hospitation/pdfreport", method = RequestMethod.GET, produces = MediaType.APPLICATION_PDF_VALUE)
    public ResponseEntity<InputStreamResource> hospitationPdf() throws IOException {

        //    List<City> cities = (List<City>) cityService.findAll();
        Optional<Hospitation> hospitation = hospitationRepository.findById(1L);
        ByteArrayInputStream bis = null;
        if (hospitation.orElse(null) != null) {
            bis = GeneratePdfReport.hospitationReport(hospitation.orElse(null));
        } else {
            return null;
        }

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition", "inline; filename=citiesreport.pdf");

        return ResponseEntity.ok().headers(headers).contentType(MediaType.APPLICATION_PDF)
                .body(new InputStreamResource(bis));
    }

    @RequestMapping(value = "/hospitationRaport/pdfreport", method = RequestMethod.GET, produces = MediaType.APPLICATION_PDF_VALUE)
    public ResponseEntity<InputStreamResource> hospitationRaportPdf() throws IOException {

        //    List<City> cities = (List<City>) cityService.findAll();
        List<Hospitation> hospitation = hospitationRepository.findAllBySemestr("Letni2022");

        ByteArrayInputStream bis = GeneratePdfReport.hospitationSemestrReport(hospitation);


        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition", "inline; filename=citiesreport.pdf");

        return ResponseEntity.ok().headers(headers).contentType(MediaType.APPLICATION_PDF)
                .body(new InputStreamResource(bis));
    }


    @GetMapping("/hospitationResult")
    public String getHospitationResult(Model model) {
        model.addAttribute("hospitations", hospitationRepository.findAll());

        //  model.addAttribute("form", new Form());

        return hospitationResultPage;
    }
}
