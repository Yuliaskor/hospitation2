package com.example.hospitationWeb.web;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("")
public class InfoController {
    private String raportSemestralnyPage="/admin/raportSemestralnyPage";
    private String proceduraHospitowaniaPage="/admin/proceduraHospitowania";

    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    @GetMapping("/raportSemestralny")
    public String getRaportSemestralny( Model model){
        return raportSemestralnyPage;
    }


    @GetMapping("/proceduraHospitowania")
    public String proceduraHospitowania( Model model){
        return proceduraHospitowaniaPage;
    }

}
