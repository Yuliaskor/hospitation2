package com.example.hospitationWeb.repository;

import com.example.hospitationWeb.model.Form;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FormRepository extends JpaRepository<Form, Long> {
}
