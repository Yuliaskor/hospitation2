package com.example.hospitationWeb.repository;

import com.example.hospitationWeb.model.Form;
import com.example.hospitationWeb.model.Hospitation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface HospitationRepository extends JpaRepository<Hospitation, Long> {

    List<Hospitation> findAllByHospitowanyId(String hospitowanyId);
    List<Hospitation> findAllByHospitujacyPierwszyId(String hospitujacyPierwszy);

    List<Hospitation> findAllBySemestr(String semestr);
    @Modifying
    @Query("update Hospitation h set h.form = ?1 where h.hospitowanyId = ?2")
    void setFormInfoById(Form form, Long hospitowanyId);

}
