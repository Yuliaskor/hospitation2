package com.example.hospitationWeb.repository;

import com.example.hospitationWeb.model.Courses;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CoursesRepository extends JpaRepository<Courses, Long> {

}
