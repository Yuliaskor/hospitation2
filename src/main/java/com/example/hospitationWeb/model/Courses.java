package com.example.hospitationWeb.model;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "courses")
public class Courses {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToMany
    private Set<AcademicTeacher> academicTeacher;
    private String name;

    private String courseName;

    private String countStudents;


    private String termAndPlace;

    // zrobić liste
    private String personHospitation;

    public Courses() {

    }

    public Courses(String name, String courseName, String countStudents, String termAndPlace, String personHospitation) {
        this.name = name;
        this.courseName = courseName;
        this.countStudents = countStudents;
        this.termAndPlace = termAndPlace;
        this.personHospitation = personHospitation;
    }


    public Set<AcademicTeacher> getAcademicTeacher() {
        return academicTeacher;
    }

    public void setAcademicTeacher(Set<AcademicTeacher> academicTeacher) {
        this.academicTeacher = academicTeacher;
    }
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public String getCountStudents() {
        return countStudents;
    }

    public void setCountStudents(String countStudents) {
        this.countStudents = countStudents;
    }

    public String getTermAndPlace() {
        return termAndPlace;
    }

    public void setTermAndPlace(String termAndPlace) {
        this.termAndPlace = termAndPlace;
    }

    public String getPersonHospitation() {
        return personHospitation;
    }

    public void setPersonHospitation(String personHospitation) {
        this.personHospitation = personHospitation;
    }
}
