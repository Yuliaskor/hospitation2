package com.example.hospitationWeb.model;

public enum Ocena {
    FIVE,FIVE_HALF, FOUR, THREE, TWO, ZERO
}
