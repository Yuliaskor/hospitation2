package com.example.hospitationWeb.model;


import javax.persistence.*;

@Entity
@Table(name = "form")
public class Form {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    private Long hospitationId;

    private String sporzadzenia;
    private String potwierdzenia;
    private Ocena koncowa;
    private Boolean czyZajeciaRozpoczelySiePunktualnie;
    private String opoznienieRozpoczeciaZajec;
    private Boolean czySprawdzonoObecnoscStudentow;
    private Integer liczbaZapisanych;
    private Boolean czySalaJestDostosowana;
    private String czySalaJestDostosowanaPrzyczyna;
    private Boolean czyProwadzacywWeryfikuje;
    private Boolean czyTrescZgodnaZProgramemKursu;
    private String inneUwagi;
    private Ocena tematCel;
    private Ocena wyjasnienie;
    private Ocena zaangazowanie;
    private Ocena inspiracja;
    private Ocena poprawneOdpowiedzi;
    private Ocena srodkiDydaktyczne;
    private Ocena poprawnyJezyk;
    private Ocena dynamikaGrupy;
    private Ocena atmosfera;
    private Ocena technicznesrodkiprzekazaniawiedzy;
    private Ocena aktualnaWiedza;
    private Ocena material;
    private Ocena umiejetnosciNauczania;
    private Ocena przyklady;
    private Ocena tempo;
    private Ocena przygotowanie;
    private Ocena zadania;
    private Ocena planowanie;
    private Ocena komentarzy;
    private Ocena dokumentacja;

    public Form(String opoznienieRozpoczeciaZajec) {
        this.opoznienieRozpoczeciaZajec = opoznienieRozpoczeciaZajec;
    }

    public Form(String sporzadzenia, String potwierdzenia, Ocena koncowa, Boolean czyZajeciaRozpoczelySiePunktualnie, String opoznienieRozpoczeciaZajec, Boolean czySprawdzonoObecnoscStudentow, Integer liczbaZapisanych, Boolean czySalaJestDostosowana, String czySalaJestDostosowanaPrzyczyna, Boolean czyProwadzacywWeryfikuje, Boolean czyTrescZgodnaZProgramemKursu, String inneUwagi, Ocena tematCel, Ocena wyjasnienie, Ocena zaangazowanie, Ocena inspiracja, Ocena poprawneOdpowiedzi, Ocena srodkiDydaktyczne, Ocena poprawnyJezyk, Ocena dynamikaGrupy, Ocena atmosfera, Ocena technicznesrodkiprzekazaniawiedzy, Ocena aktualnaWiedza, Ocena material, Ocena umiejetnosciNauczania, Ocena przyklady, Ocena tempo, Ocena przygotowanie, Ocena zadania, Ocena planowanie, Ocena komentarzy, Ocena dokumentacja) {
        this.sporzadzenia = sporzadzenia;
        this.potwierdzenia = potwierdzenia;
        this.koncowa = koncowa;
        this.czyZajeciaRozpoczelySiePunktualnie = czyZajeciaRozpoczelySiePunktualnie;
        this.opoznienieRozpoczeciaZajec = opoznienieRozpoczeciaZajec;
        this.czySprawdzonoObecnoscStudentow = czySprawdzonoObecnoscStudentow;
        this.liczbaZapisanych = liczbaZapisanych;
        this.czySalaJestDostosowana = czySalaJestDostosowana;
        this.czySalaJestDostosowanaPrzyczyna = czySalaJestDostosowanaPrzyczyna;
        this.czyProwadzacywWeryfikuje = czyProwadzacywWeryfikuje;
        this.czyTrescZgodnaZProgramemKursu = czyTrescZgodnaZProgramemKursu;
        this.inneUwagi = inneUwagi;
        this.tematCel = tematCel;
        this.wyjasnienie = wyjasnienie;
        this.zaangazowanie = zaangazowanie;
        this.inspiracja = inspiracja;
        this.poprawneOdpowiedzi = poprawneOdpowiedzi;
        this.srodkiDydaktyczne = srodkiDydaktyczne;
        this.poprawnyJezyk = poprawnyJezyk;
        this.dynamikaGrupy = dynamikaGrupy;
        this.atmosfera = atmosfera;
        this.technicznesrodkiprzekazaniawiedzy = technicznesrodkiprzekazaniawiedzy;
        this.aktualnaWiedza = aktualnaWiedza;
        this.material = material;
        this.umiejetnosciNauczania = umiejetnosciNauczania;
        this.przyklady = przyklady;
        this.tempo = tempo;
        this.przygotowanie = przygotowanie;
        this.zadania = zadania;
        this.planowanie = planowanie;
        this.komentarzy = komentarzy;
        this.dokumentacja = dokumentacja;
    }

    public Form() {

    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public String getSporzadzenia() {
        return sporzadzenia;
    }

    public void setSporzadzenia(String sporzadzenia) {
        this.sporzadzenia = sporzadzenia;
    }

    public String getPotwierdzenia() {
        return potwierdzenia;
    }

    public void setPotwierdzenia(String potwierdzenia) {
        this.potwierdzenia = potwierdzenia;
    }

    public Ocena getKoncowa() {
        return koncowa;
    }

    public void setKoncowa(Ocena koncowa) {
        this.koncowa = koncowa;
    }

    public Boolean getCzyZajeciaRozpoczelySiePunktualnie() {
        return czyZajeciaRozpoczelySiePunktualnie;
    }

    public void setCzyZajeciaRozpoczelySiePunktualnie(Boolean czyZajeciaRozpoczelySiePunktualnie) {
        this.czyZajeciaRozpoczelySiePunktualnie = czyZajeciaRozpoczelySiePunktualnie;
    }

    public String getOpoznienieRozpoczeciaZajec() {
        return opoznienieRozpoczeciaZajec;
    }

    public void setOpoznienieRozpoczeciaZajec(String opoznienieRozpoczeciaZajec) {
        this.opoznienieRozpoczeciaZajec = opoznienieRozpoczeciaZajec;
    }

    public Boolean getCzySprawdzonoObecnoscStudentow() {
        return czySprawdzonoObecnoscStudentow;
    }

    public void setCzySprawdzonoObecnoscStudentow(Boolean czySprawdzonoObecnoscStudentow) {
        this.czySprawdzonoObecnoscStudentow = czySprawdzonoObecnoscStudentow;
    }

    public Integer getLiczbaZapisanych() {
        return liczbaZapisanych;
    }

    public void setLiczbaZapisanych(Integer liczbaZapisanych) {
        this.liczbaZapisanych = liczbaZapisanych;
    }

    public Boolean getCzySalaJestDostosowana() {
        return czySalaJestDostosowana;
    }

    public void setCzySalaJestDostosowana(Boolean czySalaJestDostosowana) {
        this.czySalaJestDostosowana = czySalaJestDostosowana;
    }

    public String getCzySalaJestDostosowanaPrzyczyna() {
        return czySalaJestDostosowanaPrzyczyna;
    }

    public void setCzySalaJestDostosowanaPrzyczyna(String czySalaJestDostosowanaPrzyczyna) {
        this.czySalaJestDostosowanaPrzyczyna = czySalaJestDostosowanaPrzyczyna;
    }

    public Boolean getCzyProwadzacywWeryfikuje() {
        return czyProwadzacywWeryfikuje;
    }

    public void setCzyProwadzacywWeryfikuje(Boolean czyProwadzacywWeryfikuje) {
        this.czyProwadzacywWeryfikuje = czyProwadzacywWeryfikuje;
    }

    public Boolean getCzyTrescZgodnaZProgramemKursu() {
        return czyTrescZgodnaZProgramemKursu;
    }

    public void setCzyTrescZgodnaZProgramemKursu(Boolean czyTrescZgodnaZProgramemKursu) {
        this.czyTrescZgodnaZProgramemKursu = czyTrescZgodnaZProgramemKursu;
    }

    public String getInneUwagi() {
        return inneUwagi;
    }

    public void setInneUwagi(String inneUwagi) {
        this.inneUwagi = inneUwagi;
    }

    public Ocena getTematCel() {
        return tematCel;
    }

    public void setTematCel(Ocena tematCel) {
        this.tematCel = tematCel;
    }

    public Ocena getWyjasnienie() {
        return wyjasnienie;
    }

    public void setWyjasnienie(Ocena wyjasnienie) {
        this.wyjasnienie = wyjasnienie;
    }

    public Ocena getZaangazowanie() {
        return zaangazowanie;
    }

    public void setZaangazowanie(Ocena zaangazowanie) {
        this.zaangazowanie = zaangazowanie;
    }

    public Ocena getInspiracja() {
        return inspiracja;
    }

    public void setInspiracja(Ocena inspiracja) {
        this.inspiracja = inspiracja;
    }

    public Ocena getPoprawneOdpowiedzi() {
        return poprawneOdpowiedzi;
    }

    public void setPoprawneOdpowiedzi(Ocena poprawneOdpowiedzi) {
        this.poprawneOdpowiedzi = poprawneOdpowiedzi;
    }

    public Ocena getSrodkiDydaktyczne() {
        return srodkiDydaktyczne;
    }

    public void setSrodkiDydaktyczne(Ocena srodkiDydaktyczne) {
        this.srodkiDydaktyczne = srodkiDydaktyczne;
    }

    public Ocena getPoprawnyJezyk() {
        return poprawnyJezyk;
    }

    public void setPoprawnyJezyk(Ocena poprawnyJezyk) {
        this.poprawnyJezyk = poprawnyJezyk;
    }

    public Ocena getDynamikaGrupy() {
        return dynamikaGrupy;
    }

    public void setDynamikaGrupy(Ocena dynamikaGrupy) {
        this.dynamikaGrupy = dynamikaGrupy;
    }

    public Ocena getAtmosfera() {
        return atmosfera;
    }

    public void setAtmosfera(Ocena atmosfera) {
        this.atmosfera = atmosfera;
    }

    public Ocena getTechnicznesrodkiprzekazaniawiedzy() {
        return technicznesrodkiprzekazaniawiedzy;
    }

    public void setTechnicznesrodkiprzekazaniawiedzy(Ocena technicznesrodkiprzekazaniawiedzy) {
        this.technicznesrodkiprzekazaniawiedzy = technicznesrodkiprzekazaniawiedzy;
    }

    public Ocena getAktualnaWiedza() {
        return aktualnaWiedza;
    }

    public void setAktualnaWiedza(Ocena aktualnaWiedza) {
        this.aktualnaWiedza = aktualnaWiedza;
    }

    public Ocena getMaterial() {
        return material;
    }

    public void setMaterial(Ocena material) {
        this.material = material;
    }

    public Ocena getUmiejetnosciNauczania() {
        return umiejetnosciNauczania;
    }

    public void setUmiejetnosciNauczania(Ocena umiejetnosciNauczania) {
        this.umiejetnosciNauczania = umiejetnosciNauczania;
    }

    public Ocena getPrzyklady() {
        return przyklady;
    }

    public void setPrzyklady(Ocena przyklady) {
        this.przyklady = przyklady;
    }

    public Ocena getTempo() {
        return tempo;
    }

    public void setTempo(Ocena tempo) {
        this.tempo = tempo;
    }

    public Ocena getPrzygotowanie() {
        return przygotowanie;
    }

    public void setPrzygotowanie(Ocena przygotowanie) {
        this.przygotowanie = przygotowanie;
    }

    public Ocena getZadania() {
        return zadania;
    }

    public void setZadania(Ocena zadania) {
        this.zadania = zadania;
    }

    public Ocena getPlanowanie() {
        return planowanie;
    }

    public void setPlanowanie(Ocena planowanie) {
        this.planowanie = planowanie;
    }

    public Ocena getKomentarzy() {
        return komentarzy;
    }

    public void setKomentarzy(Ocena komentarzy) {
        this.komentarzy = komentarzy;
    }

    public Ocena getDokumentacja() {
        return dokumentacja;
    }

    public void setDokumentacja(Ocena dokumentacja) {
        this.dokumentacja = dokumentacja;
    }

    public Long getHospitationId() {
        return hospitationId;
    }

    public void setHospitationId(Long hospitationId) {
        this.hospitationId = hospitationId;
    }
}
