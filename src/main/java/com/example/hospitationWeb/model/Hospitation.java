package com.example.hospitationWeb.model;

import javax.persistence.*;

@Entity
@Table(name = "hospitation")
public class Hospitation {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne
    private Form form;

    @OneToOne
    private Courses courses;

    private String hospitujacyPierwszyId;

    private String hospitujacyDrugiId;

    private String hospitowanyId;

    private String hospitationDate;

    private String status;

    private String semestr;

    public Hospitation(String hospitujacyPierwszyId, String hospitujacyDrugiId, String hospitowanyId, String hospitationDate, String status, String semestr) {
        this.hospitujacyPierwszyId = hospitujacyPierwszyId;
        this.hospitujacyDrugiId = hospitujacyDrugiId;
        this.hospitowanyId = hospitowanyId;
        this.hospitationDate = hospitationDate;
        this.status = status;
        this.semestr = semestr;
    }

    public Hospitation(String hospitujacyPierwszyId, String hospitujacyDrugiId, String hospitowanyId, String hospitationDate, String status) {
        this.hospitujacyPierwszyId = hospitujacyPierwszyId;
        this.hospitujacyDrugiId = hospitujacyDrugiId;
        this.hospitowanyId = hospitowanyId;
        this.hospitationDate = hospitationDate;
        this.status = status;
    }

    public Hospitation(String hospitujacyPierwszyId,String hospitowanyId, String hospitationDate, String status) {
        this.hospitujacyPierwszyId = hospitujacyPierwszyId;
        this.hospitowanyId = hospitowanyId;
        this.hospitationDate = hospitationDate;
        this.status = status;
    }

    public Hospitation() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Form getForm() {
        return form;
    }

    public void setForm(Form form) {
        this.form = form;
    }

    public String getHospitationDate() {
        return hospitationDate;
    }

    public void setHospitationDate(String hospitationDate) {
        this.hospitationDate = hospitationDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Courses getCourses() {
        return courses;
    }

    public void setCourses(Courses courses) {
        this.courses = courses;
    }

    public String getHospitujacyPierwszyId() {
        return hospitujacyPierwszyId;
    }

    public void setHospitujacyPierwszyId(String hospitujacyPierwszyId) {
        this.hospitujacyPierwszyId = hospitujacyPierwszyId;
    }

    public String getHospitujacyDrugiId() {
        return hospitujacyDrugiId;
    }

    public void setHospitujacyDrugiId(String hospitujacyDrugiId) {
        this.hospitujacyDrugiId = hospitujacyDrugiId;
    }

    public String getHospitowanyId() {
        return hospitowanyId;
    }

    public void setHospitowanyId(String hospitowanyId) {
        this.hospitowanyId = hospitowanyId;
    }

    public String getSemestr() {
        return semestr;
    }

    public void setSemestr(String semestr) {
        this.semestr = semestr;
    }
}
