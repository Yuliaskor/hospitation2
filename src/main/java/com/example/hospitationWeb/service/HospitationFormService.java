package com.example.hospitationWeb.service;

import com.example.hospitationWeb.model.Form;
import com.example.hospitationWeb.model.Hospitation;
import com.example.hospitationWeb.repository.FormRepository;
import com.example.hospitationWeb.repository.HospitationRepository;
import org.springframework.stereotype.Service;

@Service
public class HospitationFormService {
    private final FormRepository formRepository;

    private final HospitationRepository hospitationRepository;

    public HospitationFormService(FormRepository formRepository, HospitationRepository hospitationRepository) {
        this.formRepository = formRepository;
        this.hospitationRepository = hospitationRepository;
    }

    public void saveForm(Form form, Long hospitationId){
        Hospitation hospitation = hospitationRepository.getOne(hospitationId);
        hospitation.setForm(form);
        hospitation.setStatus("Wypelniono");

        formRepository.save(form);

        hospitationRepository.save(hospitation);
    }
}
