package com.example.hospitationWeb.service;

public interface SecurityService {
    boolean isAuthenticated();
    void autoLogin(String username, String password);
}
